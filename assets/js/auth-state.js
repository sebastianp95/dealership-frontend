// take this token, and check it against your server to make sure it is a valid token
const token = sessionStorage.getItem("token");


if(token){
    // if there is a token, we allow users to create a new item and to log out, if not we allow them to log in
    document.getElementById("login-nav-item").hidden = true;
    document.getElementById("log-out-nav-item").hidden = false;
    document.getElementById("items-div").hidden = false;
    document.getElementById("add-icon").hidden = false;
    // document.getElementById("delete-icon").hidden = false;
    // document.getElementById("edit-icon").hidden = false;

}

// associate the logout functionality with clicking on the logout item on the nav bar
// document.getElementById("log-out-nav-item").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("token");
    document.getElementById("login-nav-item").hidden = false;
    document.getElementById("log-out-nav-item").hidden = true;
    document.getElementById("items-div").hidden = true;
    document.getElementById("add-icon").hidden = true;
    // document.getElementById("delete-icon").hidden = true;
    // document.getElementById("edit-icon").hidden = true;


    window.location.href = "./login.html";

}
