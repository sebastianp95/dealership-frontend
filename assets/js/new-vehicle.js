document.getElementById("new-item-form").addEventListener("submit", addNewItem);

function addNewItem(e) {
    e.preventDefault();
    const make = document.getElementById("vehicle-make").value;
    const model = document.getElementById("vehicle-model").value;
    const year = document.getElementById("vehicle-year").value;
    const price = document.getElementById("vehicle-price").value;
    const mileage = document.getElementById("vehicle-mileage").value;
    const newVehicle = {"make": make, "model": model, "year": year, "price": price, "mileage": mileage};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    ajaxCreateItem(newVehicle, indicateSuccess, indicateFailure);
}

function indicateSuccess() {
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerText = "New vehicle successfully created";
}

function indicateFailure() {
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerText = "There was an issue creating your new vehicle";
}