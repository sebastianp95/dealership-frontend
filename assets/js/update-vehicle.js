document.getElementById("update-vehicle-form").addEventListener("submit", updateVehicle);


function updateVehicle(e) {
    e.preventDefault();
    const id = document.getElementById("vehicle-id").value;
    console.log(id);
    const make = document.getElementById("vehicle-make").value;
    const model = document.getElementById("vehicle-model").value;
    const year = document.getElementById("vehicle-year").value;
    const price = document.getElementById("vehicle-price").value;
    const mileage = document.getElementById("vehicle-mileage").value;
    const newVehicle = {"id": id, "make": make, "model": model, "year": year, "price": price, "mileage": mileage};
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    ajaxUpdateItem(newVehicle, indicateSuccess, indicateFailure);

}

function indicateSuccess() {
    const message = document.getElementById("update-msg");
    message.hidden = false;
    message.innerText = "vehicle successfully updated";

}

function indicateFailure() {
    const message = document.getElementById("update-msg");
    message.hidden = false;
    message.innerText = "There was an issue updating your vehicle";
}